package main

import (
	"bufio"
	"flag"
	"log"
	"os"
)

func main() {
	filePath := getFilePath()
	outputPath := getOutputPath()
	flag.Parse()

	// Open the file to read from
	f, src_err := os.Open(*filePath)
	check(src_err)

	// Create the file to write to
	o, dest_err := os.Create(*outputPath)
	check(dest_err)

	// Write open bracket
	o.WriteString("[\n")

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		o.WriteString(scanner.Text() + ",\n")
	}
	if scan_err := scanner.Err(); scan_err != nil {
		log.Fatal(scan_err)
	}

	// Write open bracket
	o.WriteString("]")

	f.Close()
	o.Sync()
}

func getFilePath() *string {
	return flag.String("f", "/dev/null", "Path to log file")
}

func getOutputPath() *string {
	return flag.String("o", "./log.json", "Output file location")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
